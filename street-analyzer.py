try:
	from fuzzywuzzy import fuzz
	import pandas as pd
	import argparse
	import re
except ImportError as err:
	print err
	print 'please execute:\n\tpip install fuzzywuzzy pandas argparse'
	exit()

def count_match(addr1, addr2):
	count = 0
	for word1 in re.split(',|-| ', addr1):
		for word2 in re.split(',|-| ', addr2):
			if fuzz.token_set_ratio(word1, word2) is 100:
				count += 1
	return count
	
def count_typo(addr1, addr2):
	count = 0
	for word1 in re.split(',|-|~| ', addr1):
		for word2 in re.split(',|-|~| ', addr2):
			if 80 < fuzz.token_set_ratio(word1, word2) < 100:
				count += 1
	return count
	
if __name__ == "__main__":
	helps = argparse.ArgumentParser(description="Mendapatkan semua score, jumlah kata yang typo (salah ketik), dan jumlah kata yang match (sama) dari 2 kalimat")
	helps.add_argument("file", type=str, help="file yang berisi nama jalan yang ingin dianalisa (format file .csv)")
	
	arg = helps.parse_args()
	
	df = pd.read_csv(arg.file)
	
	df.loc[:,'score'] = df.apply(lambda x: fuzz.token_set_ratio(x[0],x[1]), axis='columns')
	df.loc[:,'match'] = df.apply(lambda x: count_match(x[0],x[1]), axis='columns')
	df.loc[:,'typo'] = df.apply(lambda x: count_typo(x[0],x[1]), axis='columns')
	
	df.to_csv(arg.file)